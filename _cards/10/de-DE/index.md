---
title: 'Aerosole'
backDescription: 'Ein Aerosol ist ein Gas, in dem winzige (feste oder flüssige) Teilchen schweben, die Aerosolpartikel genannt werden. Anthropogene Emissionen (zb bei Fabriken) führen zu Aerosolbildung, also lokalen Verschmutzungen. Sie sind gesundheitsschädlich und leiste'
lot: 3
num: 10
---
