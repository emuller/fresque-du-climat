---
title: 'Aérosols'
backDescription: 'Rien à voir avec les bombes aérosols. Les aérosols sont une pollution locale qui vient de la combustion imparfaite des énergies fossiles. _x000B_Ils sont mauvais pour la santé et ils ont par ailleurs une contribution négative au forçage radiatif (ils refroidiss'
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_10_a%C3%A9rosols'
youtubeCode: 'XRAKRtOQ_Fg'
instagramCode: 'COQbQy2o6Px'
lot: 3
num: 10
---

Cette carte mérite d'être supprimée la plupart du temps, sauf à ce que les participants aient un bon niveau, qu'ils aient du temps, et que l'animateur maitrise le sujet.

Les aérosols sont des particules solides ou liquides tellement petites que leur vite
