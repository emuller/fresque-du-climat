---
title: 'Aerosoler'
backDescription: 'Aerosoler er en type lokal forurensning som kommer fra forbrenning av fossil brensel. De er helseskadelige og bidrar negativt til strålingspådrivet (de avkjøler klimaet).'
lot: 3
num: 10
---
