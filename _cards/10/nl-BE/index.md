---
title: "Aerosols"
backDescription: "Deze luchtvervuiling wordt meestal veroorzaakt door industrie en transport en in mindere mate door landbouw en bosbranden.
Sommige aerosols reflecteren het zonlicht en hebben daarom een koelende werking op het klimaat."
lot: 3
num: 10
---
