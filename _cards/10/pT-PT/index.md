---
title: 'Aerossóis'
backDescription: 'Nada relacionado com sprays de aerossol. Os aerossóis são poluição de base local, proveniente das mesmas fábricas e tubos de escape que emitem o CO2. São nocivos à saúde e contribuem negativamente para o Forçamento Radiativo (arrefecem o clima).'
lot: 3
num: 10
---
