---
title: 'Concentration of CO2 (ppm)'
backDescription: 'About half of our CO2 emissions are captured by natural carbon sinks. The other half remains in the atmosphere. The concentration of CO2 in the air has increased from 280 to 415 ppm (parts per million) over the past 150 years. This is higher than at any p'
wikiUrl: 'https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_11_concentration_of_co2'
youtubeCode: 'cUPYgpbTBAA'
instagramCode: ''
lot: 2
num: 11
---

CO2 measurements have been taken since 1958 in Hawaii, on Big Island, on the flanks of the Mauna Loa volcano. They were initiated by Charles Keeling. In the blue scenario (2°C) they increase until 2040-2050, then they decrease because emissions will have been reduced so much that natural sinks no longer absorb them.
