---
title: 'Concentration en CO2 (ppm)'
backDescription: "Une fois que la moitié de nos émissions _x000B_de CO2 a été captée par les puits naturels, l'autre moitié reste dans l'atmosphère. La concentration en CO2 dans l'atmosphère est passée de 280 à 415 ppm (parties par millions) en 150 ans. Il faut remonter à trois"
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_11_concentration_en_co2'
youtubeCode: 'rkUfk9-kito'
instagramCode: ''
lot: 2
num: 11
---

Des mesures de CO2 ont lieu depuis 1958 à Hawaï, sur l'île de Big Island, sur les flans du volcan Mauna Loa. Elles ont été lancées par Charles Keeling. Dans le scénario bleu (2°C) elles augmentent jusqu'en 2040-2050 puis elles baissent car on a tellement diminué les émissions que les puits naturels n'absorbent plus.
