---
title: 'CO2-konsentrasjon (ppm)'
backDescription: 'Rundt halvparten av våre CO2-utslipp fanges av naturlige karbonsluker. Den andre halvparten forblir i atmosfæren; CO2-konsentrasjonen i atmosfæren har økt fra 280 til 410 ppm (andeler per million) i løpet av 150 år.'
lot: 2
num: 11
---
