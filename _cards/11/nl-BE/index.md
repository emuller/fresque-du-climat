---
title: 'Concentratie van CO2 (ppm)'
backDescription: 'Ongeveer de helft van de CO2 emissies wordt in diverse koolstofreservoirs opgeslagen. De andere helft blijft in de atmosfeer hangen.
    De concentratie van CO2 in de lucht is van 280 ppm naar 410 ppm gestegen.'
lot: 2
num: 11
---
