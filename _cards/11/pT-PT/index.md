---
title: 'Concentração de CO2 (ppm)'
backDescription: 'Cerca de metade das nossas emissões de CO2 são capturadas por sumidouros de carbono naturais. A outra metade permanece na atmosfera. A concentração de CO2 no ar aumentou de 280 para 415 ppm (partes por milhão) ao longo dos últimos 150 anos. Isto é mais el'
lot: 2
num: 11
---
