---
title: 'Koldioxidhalt (ppm)'
backDescription: 'Ungefär hälften av våra koldioxidutsläpp fångas av naturliga kolsänkor medan den andra halvan stannar i atmosfären. Koncentrationen av koldioxid i luften har de senaste 150 åren ökat från 280 till 415 ppm (parts per million). Det är den högsta koldioxidha'
lot: 2
num: 11
---
