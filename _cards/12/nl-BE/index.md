---
title: 'Koolstofreservoirs'
backDescription: 'De helft van de CO2 die we elk jaar uitstoten wordt opgeslagen in koolstofreservoirs.
    - 1/4 door de vegetatie (via fotosynthese)
    - 1/4 bij de oceaan
    De resterende helft blijft in de atmosfeer'
lot: 2
num: 12
---
