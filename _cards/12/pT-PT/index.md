---
title: "Sumidouros de Carbono"
backDescription: "Metade do CO2 que emitimos todos os anos é absorvido pelos sumidouros de carbono:
- ¼ pela vegetação (através da fotossíntese)
- ¼ pelo oceano.
A metade restantes (1/2) permanece na atmosfera."
lot: 2
num: 12
---
