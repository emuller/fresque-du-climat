---
title: "Rezerwuary węgla"
backDescription: "Połowę emitowanego przez nas rocznie CO2 pochłaniają naturalne rezerwuary węgla:
- roślinność w procesie fotosyntezy (1/4)
- oceany (1/4)
Reszta (1/2) pozostaje w atmosferze."
lot: 2
num: 12
---
