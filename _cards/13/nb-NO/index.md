---
title: 'Forsterket drivhuseffekt'
backDescription: 'Drivhuseffekten er en naturlig prosess, og den viktigste naturlige klimagassen er vanndamp. Uten drivhuseffekten ville planeten vært 33°C kaldere. Men CO2 og de andre klimagassene knyttet til menneskelige aktiviteter forsterker den naturlige drivhuseffekt'
lot: 1
num: 13
---
