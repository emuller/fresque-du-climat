---
title: 'Dodatkowy efekt cieplarniany'
backDescription: 'Efekt cieplarniany to zjawisko naturalne. Podstawowym gazem cieplarnianym jest para wodna. Bez efektu cieplarnianego temperatura powierzchni Ziemi byłaby o 33°C niższa. Jednak emisje CO2 i innych gazów cieplarnianych, związane z działalnością człowieka, z'
lot: 1
num: 13
---
