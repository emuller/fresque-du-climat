---
title: 'Ytterliggare växthuseffekt'
backDescription: 'Växthuseffekten är ett naturligt fenomen (en huvudsaklig växthusgas är vattenånga). Utan växthuseffekten skulle planeten vara 33°C kallare och liv som vi känner det idag skulle inte vara möjligt. Men, koldioxid och andra växthusgaser som kommer från mänsk'
lot: 1
num: 13
---
