---
title: 'Bilancio energetico'
backDescription: "Questo grafico spiega dove va l'energia che si accumula sulla terra a causa del forzante radiativo : riscalda l'oceano, scioglie il ghiaccio, si dissipa nel terreno e riscalda l'atmosfera."
lot: 3
num: 14
---
