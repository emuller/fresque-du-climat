---
title: 'Energiebudget'
backDescription: 'Deze grafiek toont wat er met de geaccumuleerde energie (geaccumuleerd vanwege stralingsforcering) gebeurt: het warmt de oceanen op, doet het ijs smelten, verdwijnt in de grond en warmt de atmosfeer op.'
lot: 3
num: 14
---
