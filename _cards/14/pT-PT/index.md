---
title: 'Orçamento para Energia'
backDescription: 'Este gráfico explica os efeitos da energia acumulada na Terra (em função do Forçamento Radiativo): ela aquece o oceano, derrete o gelo, é dissipada no solo e aquece a atmosfera.'
lot: 3
num: 14
---
