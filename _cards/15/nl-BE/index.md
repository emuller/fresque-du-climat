---
title: 'Stralingsforcering'
backDescription: 'Stralingsforcering is het verschil tussen de invallende energie van de zon en de energie van de straling die door de aarde uitgezonden wordt naar de ruimte. In het vijfde IPCC Assessment Report wordt de stralingsforcering geschat op 2.3 W/m2.'
lot: 3
num: 15
---
