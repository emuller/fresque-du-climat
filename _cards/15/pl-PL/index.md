---
title: 'Wymuszanie radiacyjne'
backDescription: 'Wymuszanie radiacyjne jest miarą braku równowagi między energią, która przybywa co sekundę na Ziemię, i tą, która ją opuszcza. Jego wartość wynosi 3,8 W/m² (Watt na m²) dla efektu cieplarnianego i -1 W/m² dla aerozoli, czyli dla całości 2,8 W/m².'
lot: 3
num: 15
---
