---
title: 'Strålningsdrivning'
backDescription: 'Strålningsdrivning är skillnaden mellan mängden energi som tillkommer på jorden varje sekund, och den energi som lämnar jorden. Den är beräknad till totalt 2.8 W/m² (Watt per kvadratmeter): 3.8 W/m² från växthuseffekten och -1 W/m² från aerosoler.'
lot: 3
num: 15
---
