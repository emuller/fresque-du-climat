---
title: 'Scioglimento dei ghiacciai'
backDescription: "Quasi tutti i ghiacciai hanno perso massa. Centinaia sono già scomparsi. Questi ghiacciai hanno un ruolo regolatore nell'approvvigionamento di acqua dolce."
lot: 3
num: 16
---
