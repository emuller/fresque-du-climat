---
title: 'Het smelten van gletsjers'
backDescription: 'Vrijwel alle gletsjers trekken zich terug en honderden zijn al helemaal verdwenen. De gletsjers spelen een belangrijke rol als bron van drinkwater.'
lot: 3
num: 16
---
