---
title: 'Derretimento dos glaciares'
backDescription: 'Quase todos os glaciares perderam massa. Centenas já desapareceram. Estes glaciares desempenham um papel regulador no fornecimento de água doce.'
lot: 3
num: 16
---
