---
title: 'Buzulların erimesi'
backDescription: "Dünya'daki hemen hemen bütün buzullar küçülmüştür, yüzlercesi de yok olmuştur. Bu buzullar, temiz su kaynakları açısından oldukça büyük önem taşımaktadır."
lot: 3
num: 16
---
