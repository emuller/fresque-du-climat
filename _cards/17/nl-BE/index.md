---
title: 'Toename van de watertemperatuur'
backDescription: 'De zeeën en oceanen absorberen 93% van de energie geaccumuleerd op de aarde. De temperatuur van de oceanen stijgt, vooral in de bovenste lagen, en door deze stijging zetten de oceanen uit.'
lot: 3
num: 17
---
