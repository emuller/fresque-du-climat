---
title: 'Aumento da temperatura da água'
backDescription: 'O oceano absorve 91% da energia acumulada na Terra. À medida que aquece, a água expande.'
lot: 3
num: 17
---
