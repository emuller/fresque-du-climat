---
title: 'Ökning av vattentemperaturen'
backDescription: 'Havet absoberar 91% av den energi som ackumulerats på jorden. Därför ökar dess temperatur, särskilt i de övre lagren. Vatten expanderar när det blir varmare.'
lot: 3
num: 17
---
