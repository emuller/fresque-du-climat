---
title: 'Scioglimento della banchisa'
backDescription: "Lo scioglimento della banchisa non è responsabile dell'aumento del livello dell'acqua (un cubetto di ghiaccio che si scioglie in una bevanda non fa traboccare il bicchiere). Tuttavia, quando si scioglie, lascia il posto a superfici più scure che assorbono"
lot: 1
num: 18
---
