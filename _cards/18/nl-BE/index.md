---
title: 'Smeltend zee-ijs'
backDescription: 'Het smelten van zee-ijs is niet verantwoordelijk voor de stijging van de zeespiegel (een ijsblokje dat smelt in een glas water zorgt er niet voor dat het water het glas overstroomt). Maar als zee-ijs smelt, laat het ruimte voor een veel donkerder element'
lot: 1
num: 18
---
