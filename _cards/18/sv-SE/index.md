---
title: 'Smältning av havsis'
backDescription: 'Smältning av havsis bidrar inte till att höja havsnivån (ytnivån i ett glas vatten stiger inte när en isbit i glaset smälter).'
lot: 1
num: 18
---
