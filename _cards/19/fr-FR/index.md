---
title: Fonte des calottes glaciaires
backDescription: Les calottes glaciaires sont le Groënland et l'Antarctique. Si
  elles fondaient intégralement, cela représenterait une augmentation du niveau
  de la mer de 7m pour le Groënland, et de 54m pour l'Antarctique. Durant la
  dernière ère glaciaire, les calottes ét
wikiUrl: https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_19_fonte_des_calottes_glaciaires
youtubeCode: Btoz2BU9PFo
instagramCode: CPqYtRrI2A-
lot: 3
num: 19
---

Ces illustrations représentent le gain ou la perte de masse des calottes. En bleu le gain de masse (parce qu'il neige davantage) et en rouge les pertes (les glaciers s'écoulent plus vite vers l'océan).

L'épaisseur des calottes est de l'ordre de 3000m.