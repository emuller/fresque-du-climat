---
title: 'Scioglimento delle calotte glaciali'
backDescription: "Le calotte glaciali sono la Groenlandia e l'Antartide. Se si sciogliessero completamente, provocherebbero un aumento del livello d'acqua di 7m per la Groenlandia e 54m per l'Antartide. Durante l'ultima era glaciale, le calotte erano così estese che il liv"
lot: 3
num: 19
---
