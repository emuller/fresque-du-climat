---
title: 'Smelting av innlandsis'
backDescription: 'Innlandsis er isbreer som dekker et større landområde. I dag er det kun Grønland og Antarktis som er dekket av innlandsis. Om de skulle smelte helt ville det føre til en havnivåstigning på 7 meter for Grønland og 54 meter for Antarktis. I den siste istide'
lot: 3
num: 19
---
