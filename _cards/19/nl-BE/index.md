---
title: 'Smeltende ijskappen'
backDescription: 'Op Aarde bevinden zich twee grote ijskappen (of gletsjers), namelijk in Groenland en op Antarctica. Als de Antarctische en de Groenlandse ijskappen helemaal zouden smelten, zou de zeespiegel door Groenland met 7m en door Antarctica met 54m stijgen. Tijden'
lot: 3
num: 19
---
