---
title: 'Topnienie lądolodów'
backDescription: 'Lądolody to pokrywy lodowe na Grenlandii i Antarktydzie. Gdyby całość ich masy uległa stopnieniu, poziom wód podniósłby się o 7 m w przypadku Grenlandii i o 54 m w przypadku Antarktydy. W czasie ostatniego zlodowacenia obszar pokryty lodem był tak rozległ'
lot: 3
num: 19
---
