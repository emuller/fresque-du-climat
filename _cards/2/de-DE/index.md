---
title: 'Industrie'
backDescription: 'Die Industrie nutzt fossile Brennstoffe und Strom. Sie ist für 40% der Treibhausgasemissionen verantwortlich.'
lot: 2
num: 2
---
