---
title: 'Industry'
backDescription: 'Industry uses fossil fuels and electricity. It accounts for 40% of greenhouse gas emissions.'
lot: 2
num: 2
---
