---
title: "Industrie"
backDescription: "L'industrie utilise des énergies fossiles et de l'électricité.
Elle représente 40% des Gaz à Effet de Serre (GES)."
wikiUrl: "https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_2_industrie"
youtubeCode: "4LG5GMiCcR0"
instagramCode: "CKmDdUKoIkp"
lot: 2
num: 2
---

Il s'agit de la fabrication de l'ensemble des biens de consommation. L'industrie regroupe un très grand nombre de secteurs industriels différents dont les plus importants en émissions de GES sont la papeterie, le ciment, l'acier, l'aluminium, la chimie. Pour réduire les émissions de l'industrie, la solution consiste à allonger la durée de vie des produits, voire à réduire leur consommation.
