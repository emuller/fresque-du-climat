---
title: 'Industrie'
backDescription: 'De industrie gebruikt fossiele brandstoffen en elektriciteit en is verantwoordelijk voor 40% van de totale uitstoot van broeikasgassen.'
lot: 2
num: 2
---
