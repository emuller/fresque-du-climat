---
title: 'Indústria'
backDescription: 'A indústria utiliza combustíveis fósseis e eletricidade. Ela representa 40% das emissões de gases de efeito estufa (GEE).'
lot: 2
num: 2
---
