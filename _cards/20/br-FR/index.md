---
backDescription: L'évaporation qui a lieu à la surface de l'océan augmente si l'eau
    et l'air se réchauffent. Cela fait plus de nuages qui feront ensuite de la pluie.
    Mais si l’évaporation a lieu sur terre, cela assèche le sol.
instagramCode: CNsNyoWorRv
lot: 3
num: 20
title: Diroll kelc'hiad an dour
wikiUrl: https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_20_perturbation_cycle_eau
youtubeCode: m16bWEIgUnI
---
