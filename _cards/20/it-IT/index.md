---
title: "Perturbazione del ciclo dell'acqua"
backDescription: "L'evaporazione che avviene sulla superfice dell'oceano aumenta se l'acqua e l'aria si riscaldano. Ciò porta alla formazione di più nuvole che causeranno la pioggia. Ma se l'evaporazione si svolge sulla terraferma, il suolo si prosciuga."
lot: 3
num: 20
---
