---
title: 'De verstoring van de waterkringloop'
backDescription:
    'Wanneer de temperatuur van de atmosfeer en de oceanen stijgt, neemt ook de verdamping van warm (zee)water toe, wat leidt tot meer wolken en neerslag.
    Door toenemende verdamping van water uit de bodem kan droogte ontstaan.'
lot: 3
num: 20
---
