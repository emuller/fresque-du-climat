---
title: 'Perturbação do ciclo da água'
backDescription: 'Os oceanos mais quentes e uma atmosfera mais quente levam a uma evaporação mais forte, causando nuvens de chuva e precipitação. A terra mais quente e uma atmosfera mais quente conduzem também a uma evaporação mais forte, causando desta vez a secura do sol'
lot: 3
num: 20
---
