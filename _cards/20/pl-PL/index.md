---
title: 'Zaburzenia obiegu wody w przyrodzie'
backDescription: 'Wraz z ocieplaniem się wody i powietrza wzrasta parowanie na powierzchni oceanów. W rezultacie tworzy się więcej chmur, z których spada deszcz. To samo zjawisko zachodzi na lądzie, ale tam powoduje wysychanie gleby.'
lot: 3
num: 20
---
