---
title: 'Störning av vattnets kretslopp'
backDescription: 'Varmare hav och en varmare atmosfär leder till en starkare avdunstning, vilket orsakar regnmoln och nederbörd. Varmare landområden och en varmare atmosfär leder också till en kraftigare avdunstning, men detta leder till att marken torkar ut.'
lot: 3
num: 20
---
