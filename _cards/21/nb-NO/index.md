---
title: 'Temperaturøkning'
backDescription: 'Her snakker vi om det globale gjennomsnittet av lufttemperaturen på jordens overflate. Den har økt med 1°C siden 1900. Avhengig av scenariene, temperaturøkningen kan nå mellom 2°C og 5°C innen 2100. Mot slutten av siste istid var den gjennomsnittlige temp'
lot: 1
num: 21
---
