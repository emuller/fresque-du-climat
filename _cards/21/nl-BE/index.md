---
title: 'Temperatuurstijging'
backDescription:
    "Hier hebben we het over de gemiddelde temperatuur van de lucht die aan het aardoppervlak gemeten wordt. Die is sinds 1900 met 1 graad gestegen.
    Tot 2100 zijn, volgens verschillende emissiescenario's, temperatuurstijgingen tussen de 2 en 5 °C boven het pr"
lot: 1
num: 21
---
