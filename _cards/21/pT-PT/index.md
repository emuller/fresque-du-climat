---
title: 'Aumento da temperatura do ar'
backDescription: 'Referimo-nos aqui à temperatura média do ar na superfície da Terra, que aumentou 1 °C desde 1900. Dependendo dos diferentes cenários, o aumento da temperatura poderá atingir 2 a 5 °C até 2100. No final da última era glacial (há 20.000 anos) a temperatura'
lot: 1
num: 21
---
