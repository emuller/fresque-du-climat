---
title: 'Wzrost temperatury'
backDescription: 'Chodzi tutaj o średnią temperaturę powietrza na powierzchni Ziemi, która od 1900 r. wzrosła już o blisko 1,2°C . Według przewidywań do 2100 r. może się podnieść o 2-5°C. W czasie ostatniego zlodowacenia (20 000 lat temu) była zaledwie o 5°C stopni niższa'
lot: 1
num: 21
---
