---
title: 'Temperaturökning'
backDescription: 'Luftens medeltemperatur på jordytan har sedan år 1900 ökat med 1.2°C. Utsläppsscenarion för framtiden förutspår att denna ökning kan nå upp emot 2 till 5°C år 2100. Under den senaste istiden för 20.000 år sedan var jordens medeltemperatur 5°C lägre än dag'
lot: 1
num: 21
---
