---
title: "Stijging van de zeespiegel"
backDescription: "De zeespiegel is sinds 1900 met 20 cm gestegen.
De zeespiegelstijging is veroorzaakt door de uitzetting van zeewater door temperatuurstijging, smeltende gletsjers en smeltende ijskappen"
lot: 1
num: 22
---
