---
title: 'Problemi di calcificazione'
backDescription: 'Se il pH diminuisce, la formazione di calcare diventa più difficile, in particolare per le conchiglie.'
lot: 4
num: 23
---
