---
title: 'Verstoorde verkalking van schelpen en koraalriffen.'
backDescription: 'Als water verzuurt, bevat het steeds minder calciumcarbonaat. Dit vertraagt de kalkvorming van koralen en schelpvormende soorten.'
lot: 4
num: 23
---
