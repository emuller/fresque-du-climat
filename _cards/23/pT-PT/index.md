---
title: 'Problemas de calcificação'
backDescription: 'Se o pH baixar, a formação de exoesqueletos (ou conchas calcárias) torna-se mais difícil.'
lot: 4
num: 23
---
