---
title: 'Spowolnienie wapnienia'
backDescription: 'Kiedy pH obniża się, utrudnia to powstawanie wapienia (w szczególności wapiennych muszli).'
lot: 4
num: 23
---
