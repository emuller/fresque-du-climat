---
title: 'Spomaľovanie kalcifikácie'
backDescription: 'Keď pH klesne, kalcifikácia (tvorba vápenca a najmä živočíšnych vápenatých schránok) sa sťažuje.'
lot: 4
num: 23
---

Spomaľovanie kalcifikácie
