---
title: "Acidification de l'océan"
backDescription: "Quand le CO2 se dissout dans l'océan, il se transforme en des ions acides (H2CO3 puis HCO3-). Cela a pour effet d'acidifier l'océan (le pH baisse)."
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_24_acidification_oc%C3%A9an'
youtubeCode: 'XlaLH13Pups'
instagramCode: ''
lot: 2
num: 24
---

L'acidification de l'océan est parfois appelée "l'autre problème du carbone". En effet, ce n'est pas à proprement parler une conséquence du changement climatique, mais une autre conséquence des émissions de CO2.
