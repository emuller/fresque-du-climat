---
title: 'Havforsuring'
backDescription: 'Når CO2 løses opp i sjøvann dannes det syreioner (H2CO3 og HCO3-). Dette fører til havforsuring (pH-verdien går ned).'
lot: 2
num: 24
---
