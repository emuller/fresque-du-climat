---
title: 'Verzuring van oceanen'
backDescription: 'Wanneer koolstofdioxide (CO2) in water (H2O) terechtkomt, lost het op en vormt het waterstofcarbonaat (H2CO3). Dit carbonaat heeft echter zwakke bindingen en zal één of twee waterstofionen (H+) afscheiden. Door de hogere concentratie waterstofionen daalt'
lot: 2
num: 24
---
