---
title: 'Acidificação dos oceanos'
backDescription: 'Quando o CO2 se dissolve no oceano, transforma-se em iões ácidos (H2CO3 e HCO3-). O efeito dessa transformação é a acidificação do oceano (o pH diminui).'
lot: 2
num: 24
---
