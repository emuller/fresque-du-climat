---
title: 'Zakwaszenie oceanów'
backDescription: 'Pochłonięty przez ocean CO2, rozpuszczając się w wodzie zmienia się w jony o charakterze kwasowym (H2CO3 i HCO3-). Powoduje to wzrost kwasowości oceanów (obniżenie pH).'
lot: 2
num: 24
---
