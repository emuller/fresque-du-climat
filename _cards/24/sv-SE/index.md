---
title: 'Havsförsurning'
backDescription: 'När koldioxid löses upp i haven, omvandlas det till syrajoner (H2CO3 and HCO3-). Detta leder till en försurning av haven (pH-halten minskar).'
lot: 2
num: 24
---
