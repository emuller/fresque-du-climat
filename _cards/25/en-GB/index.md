---
title: 'Terrestrial Biodiversity'
backDescription: 'Animals and plants are affected by the changes in temperature and the disruption of the water cycle. They may migrate or go extinct. Some may thrive and proliferate.'
wikiUrl: 'https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_25_terrestrial_biodiversity'
youtubeCode: 'oa5rHgw2EE0'
instagramCode: ''
lot: 4
num: 25
---

Today, the Earth's biodiversity is being undermined above all by factors other than climate change, such as deforestation, disappearance of natural habitats, use of pesticides and various forms of pollution. However, climate change will largely contribute to the disappearance of species in the coming decades.
