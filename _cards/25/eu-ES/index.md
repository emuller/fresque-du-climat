---
title: 'Lurreko bioaniztasuna'
backDescription: 'Animaliek eta landareek, tenperaturan eta uraren zikloan gertatzen diren aldaketak pairatzen dituzte: lekuz aldatzen dira edo desagertzen dira (edo, bakanago, ugaritzen dira).'
lot: 4
num: 25
---
