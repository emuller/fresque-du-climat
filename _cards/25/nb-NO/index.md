---
title: 'Biologisk mangfold på land'
backDescription: 'Dyr og planter påvirkes av temperaturendringer og forstyrrelser i vannets kretsløp. De kan migrere, bli utryddet eller, noen få ganger, spre seg.'
lot: 4
num: 25
---
