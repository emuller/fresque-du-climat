---
title: 'Terrestriële biodiversiteit'
backDescription: "Veranderingen van de temperatuur en van de waterkringloop hebben grote gevolgen voor planten en dieren. Veel soorten hebben als reactie op de klimaatverandering hun verspreidingsgebied, seizoensactiviteiten, migratieschema's, omvang en/of interactie met a"
lot: 4
num: 25
---
