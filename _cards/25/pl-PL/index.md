---
title: 'Bioróżnorodność lądowa'
backDescription: 'Zmiany temperatury i zaburzenia w obiegu wody w przyrodzie wpływają na rośliny i zwierzęta: te przenoszą swoje siedliska lub zanikają (a czasem, chociaż rzadko, gwałtownie się rozmnażają).'
lot: 4
num: 25
---
