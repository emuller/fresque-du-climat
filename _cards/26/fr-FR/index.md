---
title: 'Crues'
backDescription: "La perturbation du cycle de l'eau peut amener plus d'eau ou moins d’eau. Plus d'eau, cela peut engendrer des crues (inondations dans les terres). Si le sol a été durci par une sécheresse, c'est pire car l'eau ruisselle."
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_26_crues'
youtubeCode: '43Hf5wnz3Wk'
instagramCode: ''
lot: 4
num: 26
---

Une crue est l'élévation du niveau d'un cours d'eau due aux précipitations ou à des fontes de neige ou de glace. Une crue est temporaire. Sur la carte du monde représentée par régions (hexagones) (issue 6è rapport de synthèse du GIEC), les zones vertes subissent une augmentation des précipitations. Le nombre de points représente le degré de confiance dans la contribution humaine au changement observé :... = Haut, .. = Moyen, . = Faible. Impact et niveau de confiance par région du monde.
