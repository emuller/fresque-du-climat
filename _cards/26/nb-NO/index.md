---
title: 'Elveflom'
backDescription: 'Forstyrrelser i vannets kretsløp kan føre til mer eller mindre vann. Mer vann kan føre til flom. Hvis jordoverflaten på forhånd har tørket ut på grunn av tørke, kan flommen forverres fordi vannet renner av i stedet for å trekke ned i jorda.'
lot: 4
num: 26
---
