---
title: "Rivieroverstromingen"
backDescription: "Verstoring van de waterkringloop kan een tekort of een overvloed aan water opleveren.
Te veel water kan rivieroverstromingen veroorzaken. Een droge bodem maakt het probleem nog erger, omdat het water dan sneller afvloeit."
lot: 4
num: 26
---
