---
title: 'Biodiversità marina'
backDescription: "Essendo gli pteropodi e i coccolitofori alla base della catena alimentare, la loro scomparsa minaccia tutta la biodiversità marina. Il riscaldamento dell'acqua gioca un ruolo importante nell'indebolimento della biodiversità marina."
lot: 4
num: 27
---
