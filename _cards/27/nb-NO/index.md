---
title: 'Marint biologisk mangfold'
backDescription: 'Pteropoder og kalkflagellater befinner seg nederst i havets næringkjede. Hvis de skulle forsvinne, vil det true alt marint biologisk mangfold. Økning i havtemperaturen truer også det biologiske mangfoldet i havet.'
lot: 4
num: 27
---
