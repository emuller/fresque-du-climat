---
title: 'Mariene biodiversiteit'
backDescription: 'Pteropoda en coccolithoforen staan aan de basis van de voedselketen in de oceaan. Daarom wordt, als ze verdwijnen, de hele mariene biodiversiteit bedreigd. De opwarming van het oceaanwater bedreigt ook de mariene biodiversiteit.'
lot: 4
num: 27
---
