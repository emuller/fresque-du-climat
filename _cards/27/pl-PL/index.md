---
title: 'Bioróżnorodność morska'
backDescription: 'Skrzydłonogi i kokolitofory stanowią podstawę łańcucha żywnościowego, dlatego ich zanikanie zagraża całej bioróżnorodności morskiej. Wzrost temperatury wody także zagraża morskiej bioróżnorodności.'
lot: 4
num: 27
---
