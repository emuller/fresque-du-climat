---
title: 'Betegségek vektorai'
backDescription: 'A globális felmelegedés miatt az állatok vándorolnak. Némelyikük kórokozókat hordoz magával és olyan területeket is elérhet, ahol a lakosság nem immunis az adott kórokozóval szemben.'
lot: 5
num: 28
---
