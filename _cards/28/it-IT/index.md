---
title: 'Vettori di malattia'
backDescription: 'Con il riscaldamento, gli animali migrano. Alcuni sono vettori di malattie e raggiungono zone in cui le popolazioni non sono immunizzate.'
lot: 5
num: 28
---
