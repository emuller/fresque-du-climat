---
title: 'Vectoren voor ziekteoverdracht'
backDescription: 'Onder invloed van klimaatverandering zullen diersoorten migreren naar nieuwe gebieden; sommige soorten dragen ziektes waartegen de inheemse populaties minder of geen weerstand hebben.'
lot: 5
num: 28
---
