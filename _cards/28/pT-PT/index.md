---
title: 'Vetores de Doenças'
backDescription: 'Com o aquecimento global, os animais migram. Alguns são portadores de doenças e podem atingir áreas onde as populações não são imunes a essas doenças.'
lot: 5
num: 28
---
