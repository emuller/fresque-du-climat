---
title: 'Pteropoda und Coccolithophorida'
backDescription: 'Pteropoda sind Zooplankton und Coccolithophorida sind Phytoplankton. Diese Mikroorganismen haben eine Kalkschale.'
lot: 4
num: 29
---
