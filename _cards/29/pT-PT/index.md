---
title: 'Pterópodes e cocolitóforos'
backDescription: 'Pterópodes são zooplânctons e cocolitóforos são fitoplânctons. Estes organismos possuem uma concha calcária.'
lot: 4
num: 29
---
