---
title: 'Skrzydłonogi i kokolitofory'
backDescription: 'Skrzydłonogi to przedstawiciele zooplanktonu, a kokolitofory – fitoplanktonu. Te mikroorganizmy wykształcają wapienne szkielety.'
lot: 4
num: 29
---
