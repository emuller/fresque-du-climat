---
title: 'Utilisation des bâtiments'
backDescription: "L’utilisation du bâtiment (logement et services) utilise des énergies fossiles et de l'électricité. Cela représente 20% des Gaz à Effet de Serre (GES)."
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_3_utilisation_des_b%C3%A2timents'
youtubeCode: 'rxHxR0Ald3I'
instagramCode: ''
lot: 2
num: 3
---

On parle ici de l'utilisation des bâtiments et non de leur construction (qui, elle, entre dans le secteur de l'industrie). Chauffage, climatisation, éclairage, électronique, etc. Le gros sujet, sous nos latitudes, c'est l'isolation thermique des bâtiments. En ce qui concerne la construction dans le neuf, il est important de construire des bâtiment bien isolés. Mais l'enjeu est limité car les normes dans le neuf sont bien plus contraignantes que par le passé et on ne construit chaque année qu'une petite partie (1%) du parc déjà construit. L'enjeu est donc beaucoup plus dans la rénovation thermique des bâtiments.
