---
title: "Uso degli edifici"
backDescription: "Gli edifici (abitativi e commerciali) utilizzano combustibili fossili ed elettricità. Sono responsabili del 20% delle emissioni di gas serra (GHG).
"
lot: 2
num: 3
---
