---
title: 'Bruk av bygninger'
backDescription: 'Bygningssektoren (boliger og kommersielle bygg) bruker fossil energi og elektrisitet. Den står for 20 % av de totale klimagassutslippene.'
lot: 2
num: 3
---
