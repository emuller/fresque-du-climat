---
title: 'Gebruik van gebouwen'
backDescription: 'Gebouwen (woningen en winkels) maken gebruik van fossiele brandstoffen en elektriciteit. Deze sector is verantwoordelijk voor 20% van de totale uitstoot van broeikasgassen (BKG).'
lot: 2
num: 3
---
