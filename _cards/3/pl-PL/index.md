---
title: 'Eksploatacja budynków'
backDescription: 'Eksploatacja budynków (mieszkalnych i usługowych) powoduje zużycie paliw kopalnych oraz energii elektrycznej. Odpowiada za 20% emisji gazów cieplarnianych.'
lot: 2
num: 3
---
