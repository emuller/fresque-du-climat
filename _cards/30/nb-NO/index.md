---
title: 'Tørke'
backDescription: 'Forstyrrelser i vannets kretsløp kan føre til mer eller mindre vann. Ved mindre vann blir det tørke. Tørke vil sannsynligvis skje oftere i fremtiden.'
lot: 4
num: 30
---
