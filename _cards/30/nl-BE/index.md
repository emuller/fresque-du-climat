---
title: 'Droogtes'
backDescription: 'Verstoringen van de waterkringloop kunnen droogte en overstroming veroorzaken. Volgens het IPCC (Intergovernmental Panel on Climate Change) zou droogte in de toekomst vaker voorkomen.'
lot: 4
num: 30
---
