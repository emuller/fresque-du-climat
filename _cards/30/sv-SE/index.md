---
title: 'Torka'
backDescription: 'Störningar av vattnets kretslopp kan leda till ökade eller minskade mängder vatten. När störningarna leder till mindre vatten kan torka uppstå. Torka kommer sannolikt att bli mer vanligt förekommande i framtiden.'
lot: 4
num: 30
---
