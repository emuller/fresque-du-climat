---
title: 'Ur geza baliabideak'
backDescription: 'Prezipitazio aldaketen eta glaziarren desagertzearen ondorioz, ur geza baliabideak kaltetuak dira. Alta, haiek dute ibaien emaria erregulatzen.'
lot: 5
num: 31
---
