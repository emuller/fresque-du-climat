---
title: 'Édesvízi források'
backDescription: 'Az édesvíz forrásaira hatással van az esőzések változása és a folyók vízhozamát szabályozó gleccserek eltűnése.'
lot: 5
num: 31
---
