---
title: 'Risorse di acqua dolce'
backDescription: "Le risorse di acqua dolce sono influenzate dai cambiamenti nelle precipitazioni e dalla scomparsa dei ghiacciai che svolgono un ruolo regolatore della portata dei corsi d'acqua."
lot: 5
num: 31
---
