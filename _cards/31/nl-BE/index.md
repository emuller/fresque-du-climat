---
title: 'Zoetwaterbronnen'
backDescription: 'Zoetwaterbronnen worden beïnvloed door veranderingen in de regenval en door het verdwijnen van gletsjers. Die spelen een regulerende rol in de waterstromen.'
lot: 5
num: 31
---
