---
title: 'Zasoby wód słodkich'
backDescription: 'Zasoby wód słodkich dotknięte są skutkami zmienności ilości opadów i procesu topnienia lodowców, które odgrywają rolę regulatora w natężeniu przepływów.'
lot: 5
num: 31
---
