---
title: 'Pokles zemědělské produkce'
backDescription: 'Zemědělská produkce může být ovlivněna teplotou, suchem, extrémními povětrnostními podmínkami, záplavami a zaplavováním mořem (např. delta Nilu).'
lot: 5
num: 32
---
