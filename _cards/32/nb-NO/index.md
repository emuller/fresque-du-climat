---
title: 'Nedgang i avlingsutbyttet'
backDescription: 'Matproduksjon kan påvirkes av temperatur, tørke, ekstremvær, flom og stormflo (f.eks. Nildeltaet).'
lot: 5
num: 32
---
