---
title: 'Declínio da produção agrícola'
backDescription: 'A produção de alimentos pode ser afetada pela temperatura, secas, eventos climáticos extremos, inundações e submersão marinha (por exemplo, o Delta do Nilo).'
lot: 5
num: 32
---
