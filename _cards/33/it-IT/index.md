---
title: 'Sommersioni'
backDescription: "Cicloni e perturbazioni portano vento (e di conseguenza onde di tempesta) e abbassamento di pressione. Un hectopascal in meno corrisponde a 1 cm d'acqua in più. Possono quindi provocare sommersioni (inondazioni costiere) che sono aggravate dall'aumento de"
lot: 4
num: 33
---
