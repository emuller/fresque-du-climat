---
title: 'Zalania terenów nadmorskich'
backDescription: 'Cyklony i zaburzenia pogodowe przynoszą wiatr (a więc i fale) oraz spadek ciśnienia. A każdy hektopaskal mniej oznacza 1 cm wody więcej. Cyklony mogą więc powodować zalania terenów nadmorskich, których skala będzie dodatkowo potęgowana przez wzrost poziom'
lot: 4
num: 33
---
