---
title: 'Kustöversvämningar'
backDescription: 'Cykloner och andra oväder påverkar vindar (och därför även vågor) samt leder till sänkt havsnivåtryck. En hectopascal (hPa) mindre innebär en höjning av havsnivån med 1 cm. Därför kan cykloner orsaka kustöversvämningar, förstärkta av den höjda havsnivån s'
lot: 4
num: 33
---
