---
backDescription: Les cyclones s’alimentent de l’énergie des eaux chaudes à la surface
    de l’océan. Leur puissance a augmenté à cause du changement climatique.
instagramCode: ''
lot: 4
num: 34
title: Kelc'hwidennoù
wikiUrl: https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_34_cyclones
youtubeCode: YvJcgi3LjDY
---
