---
title: 'Cicloni'
backDescription: "I cicloni sono alimentati dall'energia delle acque calde sulla superficie degli oceani. La loro potenza è aumentata a causa del cambiamento climatico."
lot: 4
num: 34
---
