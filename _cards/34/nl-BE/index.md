---
title: 'Cyclonen'
backDescription: 'Boven warm oceaanwater kan een depressie steeds meer energie verzamelen en zo uitgroeien tot een cycloon. De intensiteit van cyclonen kan in de toekomst toenemen, doordat het oceaanwater opwarmt.'
lot: 4
num: 34
---
