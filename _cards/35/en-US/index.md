---
title: 'Forest Fires'
backDescription: 'Forest fires start more easily during droughts and heat waves.'
lot: 5
num: 35
---
