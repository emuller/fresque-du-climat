---
title: 'Heatwaves'
backDescription: 'One consequence of higher temperatures is more frequent heatwaves.'
wikiUrl: 'https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_36_heat_waves'
youtubeCode: 'cw0iqw6jcyc'
instagramCode: ''
lot: 5
num: 36
---

A heat wave is a meteorological phenomenon of abnormally high air temperatures, day and night, lasting from a few days to a few weeks, over a relatively large area.
