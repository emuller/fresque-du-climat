---
title: 'Fome'
backDescription: 'A fome pode ser causada pelo declínio nos rendimentos agrícolas ou pela redução da biodiversidade marinha.'
lot: 5
num: 37
---
