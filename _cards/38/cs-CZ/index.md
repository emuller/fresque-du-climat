---
title: 'Lidské zdraví'
backDescription: 'Hladomory, migrace přenašečů nemocí, vlny veder a ozbrojené konflikty mohou ovlivňovat lidské zdraví.'
lot: 5
num: 38
---
