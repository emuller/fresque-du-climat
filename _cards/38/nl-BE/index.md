---
title: 'Menselijke gezondheid'
backDescription: 'Hongersnoden, de verschuiving van vectoren voor ziekteoverdracht, hittegolven en gewapende conflicten hebben invloed op de menselijke gezondheid.'
lot: 5
num: 38
---
