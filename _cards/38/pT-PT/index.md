---
title: 'Saúde Humana'
backDescription: 'Fomes, deslocamento de vetores de doenças, ondas de calor e conflitos armados podem afetar a saúde humana.'
lot: 5
num: 38
---
