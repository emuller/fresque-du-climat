---
title: 'Ľudské zdravie'
backDescription: 'Hlad, premiestňovanie prenášačov chorôb, vlny horúčav a ozbrojené konflikty môžu ovplyvniť ľudské zdravie.'
lot: 5
num: 38
---
