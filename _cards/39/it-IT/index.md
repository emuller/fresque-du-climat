---
title: 'Rifugiati climatici'
backDescription: "Immaginate di vivere in un luogo che è miracolosamente risparmiato dal cambiamento climatico. C'è il rischio che qualche miliardo di persone voglia condividerlo con voi!"
lot: 5
num: 39
---
