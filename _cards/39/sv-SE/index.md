---
title: 'Klimatflyktingar'
backDescription: 'Föreställ dig att du bor på en plats som på ett mirakulöst sätt har undgått klimatförändringar. Miljarder människor kan komma att vilja dela den platsen med dig.'
lot: 5
num: 39
---
