---
title: 'Transporte'
backDescription: 'O setor dos transportes é muito dependente do petróleo. Representa 15% das emissões de gases de efeito estufa (GEE).'
lot: 2
num: 4
---
