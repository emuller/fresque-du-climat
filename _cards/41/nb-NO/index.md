---
title: 'Permafrost'
backDescription: 'Permafrost er fast frossen jord. Smelting av permafrost fører til oppløsing av organisk materiale som tidligere ble frosset under jorda. Dette slipper ut både metan og CO2 til atmosfæren. Etter 2 grader vil dette mest sannsynligvis øke og resultere i en n'
lot: 5
num: 41
---
