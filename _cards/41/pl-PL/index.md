---
title: 'Wieczna zmarzlina'
backDescription: 'Wieczna zmarzlina to stale zamarznięta warstwa gruntu. Zaobserwowano, że zaczyna ona rozmarzać. W efekcie zamrożona dotąd materia organiczna zaczyna się rozkładać, co powoduje uwalnianie metanu i CO2. Mamy tu do czynienia z tzw. sprzężeniem zwrotnym, podo'
lot: 5
num: 41
---
