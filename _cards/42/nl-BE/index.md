---
title: 'Methaanhydraten'
backDescription: 'Methaanhydraat, ook wel methaan-clathraat of methaanijs genoemd, is een vorm van waterijs dat grote hoeveelheden methaan bevat.
    Grote hoeveelheden clathraat komen voor in sedimenten op de bodem van de aardse oceanen. Als de temperatuur stijgt, worden dez'
lot: 5
num: 42
---
