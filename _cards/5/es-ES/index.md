---
title: 'Combustibles fósiles'
backDescription: 'Los combustibles fósiles son: el carbón, el petróleo y el gas natural. Cuando se queman, emiten CO2. Se utilizan principalmente en los edificios, el transporte y la industria.'
lot: 1
num: 5
---
