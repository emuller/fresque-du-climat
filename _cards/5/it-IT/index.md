---
title: 'Combustibili fossili'
backDescription: "I combustibili fossili sono: carbone, petrolio e gas. Sono utilizzati principalmente negli edifici, nei trasporti e nell'industria. Emettono CO2 durante la combustione."
lot: 1
num: 5
---
