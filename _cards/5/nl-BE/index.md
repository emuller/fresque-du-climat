---
title: 'Fossiele brandstoffen'
backDescription: 'Onder fossiele brandstoffen vallen onder andere aardolie, aardgas, steenkool en bruinkool.
    De industrie, de bouwsector en de transportsector verbruiken allemaal fossiele brandstoffen.
    Bij de verbranding van deze fossiele brandstoffen komt CO2 vrij.'
lot: 1
num: 5
---
