---
title: 'Paliwa kopalne'
backDescription: 'Do paliw kopalnych zalicza się węgiel, ropę naftową i gaz ziemny. Znajdują zastosowanie głównie w eksploatacji budynków, transporcie i przemyśle. Podczas spalania wytwarzają CO2.'
lot: 1
num: 5
---
