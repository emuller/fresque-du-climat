---
title: 'Déforestation'
backDescription: 'La déforestation consiste à couper ou brûler des arbres au-delà de la capacité de renouvellement de la forêt. Elle est liée à 80% à l’agriculture.'
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_6_d%C3%A9forestation'
youtubeCode: 'EMi8CNnHVmE'
instagramCode: 'CK4FDvDIYXN'
lot: 2
num: 6
---

La déforestation peut être considérée comme une activité humaine ou comme une conséquence de l'agriculture, ou encore les deux à la fois. Attention : le problème principal avec la déforestation, ce n'est pas que ça détruit des puits de carbone, c'est que ça émet du CO2 car 93% du bois déforesté est brulé sur place. C'est une différence entre flux et stock.
