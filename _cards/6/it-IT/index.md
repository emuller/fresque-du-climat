---
title: 'Deforestazione'
backDescription: "La deforestazione consiste nel tagliare o bruciare alberi oltre la capacità di rigenerazione delle foreste. Nell'80% dei casi è legata all'agricoltura."
lot: 2
num: 6
---
