---
title: 'Avskoging'
backDescription: 'Avskoging er nedhugging eller brenning av skog i et slikt omfang at gjenvekst av skogen reduseres eller opphøres. 80% av avskoging er knyttet til landbruk.'
lot: 2
num: 6
---
