---
title: 'Ontbossing'
backDescription:
    'Ontbossing is het op grote schaal kappen of verbranden van bossen door mensen. Bij ontbossing wordt het herstellingsvermogen van het bos overschreden.
    80% van de ontbossing wordt veroorzaakt door landbouw.'
lot: 2
num: 6
---
