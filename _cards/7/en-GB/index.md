---
title: 'CO2 Emissions'
backDescription: 'CO2, or carbon dioxide, is the main anthropogenic (produced by human activities) greenhouse gas in terms of emissions. These emissions come from our use of fossil fuels and from deforestation.'
wikiUrl: 'https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_7_co2_emissions'
youtubeCode: 'vA7BJ8f8t8o'
instagramCode: 'CKB_TF4HCFH'
lot: 1
num: 7
---

CO2 sources are witten in a litteral way on this card: Fossil Energy and Deforestation.
