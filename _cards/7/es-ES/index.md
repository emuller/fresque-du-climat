---
title: 'Emisiones de CO2'
backDescription: 'El CO2 es el principal GEI antrópico (de origen humano). Las emisiones de CO2 provienen de la utilización de los combustibles fósiles y de la deforestación.'
lot: 1
num: 7
---
