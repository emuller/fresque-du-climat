---
title: 'Émissions de CO2'
backDescription: 'Le CO2 est le premier gaz à effet de serre anthropique (càd émis par l’homme). Les émissions de CO2 viennent de la combustion des énergies fossiles et de la déforestation.'
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_7_%C3%A9missions_de_co2'
youtubeCode: 'ACV16QfNbok'
instagramCode: 'CKCAW03or_e'
lot: 1
num: 7
---

En couleur marron, les émissions liées à la déforestation. En couleur grise, les émissions liées à à la combustion des énergies fossiles. L'échelle verticale est en Pg C/an (Peta grammes de carbone = 10^15 grammes), ce qui est la même chose que des Gt C /an (Giga tonnes de carbone par an). Pour convertir en CO2, il faut multiplier par 3.67. La particularité de cette carte est qu'il n'y a pas l'échelle de temps. Cela peut servir d'indice pour l'associer à la carte Puit de carbone.
