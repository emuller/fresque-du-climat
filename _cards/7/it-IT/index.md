---
title: 'Emissioni di CO2'
backDescription: "La CO2 è il principale gas a effetto serra antropogenico (cioè emesso dall'uomo). Le emissioni di CO2 derivano dal uso dei combustibili fossili e dalla deforestazione."
lot: 1
num: 7
---
