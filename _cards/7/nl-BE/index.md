---
title: 'CO2-uitstoot'
backDescription: 'CO2 (of koolstofdioxide) is het belangrijkste antropogene (d.w.z. aan menselijke activiteiten gekoppelde) broeikasgas in termen van uitstoot. Deze emissies zijn afkomstig van ons gebruik van fossiele brandstoffen (verbranding) en ontbossing.'
lot: 1
num: 7
---
