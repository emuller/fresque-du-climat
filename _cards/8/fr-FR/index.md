---
title: 'Agriculture'
backDescription: "L'agriculture est responsable de l'émission d'un peu de CO2 et de beaucoup de méthane (bovins, rizières), et de protoxyde d'azote (engrais). En tout, c’est 25% des GES si on y inclut la déforestation induite."
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_8_agriculture'
youtubeCode: 'BIZ_MNIehjg'
instagramCode: 'CP8bcU4ItK5'
lot: 2
num: 8
---

L'agriculture utilise très peu d'énergie fossile, au regard des émissions d'autres GES dont elle est responsable. Elle est responsable de 80% de la déforestation. En effet, il faut des grandes surfaces pour cultiver, surtout pour nourrir les animaux d'élevage. L'agriculture est une activité humaine qui a commencé dès que le climat s'est stabilisé, au début du Néolithique il y a 10.000 ans, après la dernière déglaciation qui a elle-même duré 10.000 ans. Dès lors, l’impact de l’activité humaine sur son environnement n’a cessé de croître : il a domestiqué des espèces végétales (aujourd'hui, le riz domestiqué n’est plus capable de se reproduire sans l’intervention de l’homme), il a déforesté pour étendre les surfaces cultivées, privant les espèces animales de leur habitat naturel, et depuis la révolution verte (verte pour agricole, pas pour écologique !), il a commencé à utiliser des pesticides et des intrants nocifs pour l’environnement et pour lui-même. Cette carte regroupe aussi les activités liées à l'aquaculture (production animale ou végétale en milieu aquatique).
