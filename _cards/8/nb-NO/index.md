---
title: 'Landbruk'
backDescription: 'Landbruket slipper ikke ut mye CO2, men står for store metan- (fra kyr og rismarker) og nitrogenoksidutslipp (fra gjødsel). Totalt står landbruket for 25 % av klimagassutslippene hvis vi inkluderer utslippene fra avskoging til landbruksformål.'
lot: 2
num: 8
---
