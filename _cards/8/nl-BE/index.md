---
title: 'Landbouw'
backDescription: 'De landbouw stoot niet veel CO2 uit, maar is verantwoordelijk voor de grootschalige uitstoot van methaan of CH4 (koeien en rijstvelden) en lachgas of N2O (meststoffen) . In totaal gaat dit over 25% van de totale hoeveelheid BKG als we de extra ontbossing'
lot: 2
num: 8
---
