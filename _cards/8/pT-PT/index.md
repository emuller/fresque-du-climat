---
title: 'Agricultura'
backDescription: 'A agricultura é responsável pela emissão de uma pequena parcela de CO2, mas de uma grande parcela de metano (da pecuária e arrozais) e óxido nitroso (de fertilizantes). No total, representa 25% dos GEE emitidos, se considerarmos ainda a desflorestação ind'
lot: 2
num: 8
---
