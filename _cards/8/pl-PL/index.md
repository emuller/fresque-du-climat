---
title: 'Rolnictwo'
backDescription: 'Rolnictwo jest odpowiedzialne za emisje niewielkiej ilości CO2 oraz znacznych ilości metanu (bydło, uprawa ryżu) i podtlenku azotu (nawozy). W sumie daje to 25% gazów cieplarnianych, jeśli wliczyć skutki wylesiania.'
lot: 2
num: 8
---
