---
title: 'Poľnohospodárstvo'
backDescription: 'Poľnohospodárstvo nevypúšťa veľa CO2, ale je zodpovedné za veľké množstvo metánu (dobytok, ryžové polia) a oxidu dusného (umelé hnojivá). Celkovo predstavuje 25% skleníkových plynov ak počítame aj spôsobené odlesňovanie.'
lot: 2
num: 8
---
