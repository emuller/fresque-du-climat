---
title: 'Altri gas serra'
backDescription: "La CO2 non è l'unico gas serra. Tra i diversi gas serra troviamo anche il metano (CH4) e il protossido di azoto (N2O) (provenienti soprattutto dall'agricultura)"
lot: 2
num: 9
---
