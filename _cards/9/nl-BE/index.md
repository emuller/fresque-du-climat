---
title: 'Andere broeikasgassen.'
backDescription: 'CO2 is niet het enige broeikasgas. Twee andere belangrijke broeikasgassen zijn methaan (CH4) en stikstofdioxide (N2O) (uitstoot voornamelijk in de landbouw).'
lot: 2
num: 9
---
