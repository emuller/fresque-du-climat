---
linkId: '10_15'
fromCardId: 10
toCardId: 15
status: valid
---

Ce qui est expliqué au dos de la carte 10 et dans la nouvelle version de la carte 15 (> oct 2018) : Les aérosols refroidissent le climat. Voir les fiches thématiques sur les aérosols et sur le forçage radiatif.
