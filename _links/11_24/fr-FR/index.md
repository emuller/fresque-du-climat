---
linkId: '11_24'
fromCardId: 11
toCardId: 24
status: invalid
---

On retrouve souvent comme origine de cette carte concentration en CO2 (ppm). Or toute molécule qui se retrouve dans l'océan ne se retrouve pas dans l'atmosphère, et donc le lien le plus logique provient de la carte puits de carbone.
