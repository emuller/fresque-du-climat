---
linkId: '12_32'
fromCardId: 12
toCardId: 32
status: optional
---

Des études ont montré que les rendements augmentent avec de l'augmentation du CO2, mais que la teneur en éléments nutritifs des légumes s'en trouve réduite. En effet, les oligo-éléments ne sont pas plus abondants sous prétexte que les rendements augmentent.
