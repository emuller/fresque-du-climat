---
linkId: '1_25'
fromCardId: 1
toCardId: 25
status: optional
---

L'être humain occupe presque tout l'espace disponible sur terre, ne laissant aucune place aux animaux et aux plantes. C'est ce qu'on appelle la disparition des habitats naturels et c'est la principale cause de perte de biodiversité aujourd'hui, bien devant les causes climatiques.
