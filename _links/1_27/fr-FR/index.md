---
linkId: '1_27'
fromCardId: 1
toCardId: 27
status: optional
---

Par ce lien, on exprime toutes les dégradations que l'Homme est capable d'infliger à la vie marine comme la pollution au plastique et la surpêche. C'est hors-sujet par rapport au changement climatique, mais c'est intéressant de faire le lien quand même et l'occasion d'évoquer la Fresque Océane. Si on parle a un public âgé, on peut par exemple parler de la taille des boites de sardines qui a diminué car elles n'ont plus le temps de grandir avant d'être pêchées. Le phénomène est aggravé par le réchauffement de l'eau qui réduit l'apport en plancton.
