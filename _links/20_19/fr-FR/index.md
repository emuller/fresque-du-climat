---
linkId: '20_19'
fromCardId: 20
toCardId: 19
status: optional
---

C'est très technique, mais si on est pointilleux, la partie bleue du graphique de la carte 19 concernant l'Antarctique représente un gain de masse dû à une augmentation des précipitations. Les parties rouges représentent une perte de masse. Au total, l'Antarctique perd de la masse.
