---
linkId: '20_22'
fromCardId: 20
toCardId: 22
status: invalid
---

Eh non, plus de pluie ne va pas faire déborder l'océan ! Il est assez rare de voir ce lien, mais on ne sait jamais. Si c'est le cas, demandez aux participants d'où vient la pluie, ou encore d'où viennent les nuages...
