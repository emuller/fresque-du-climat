---
linkId: '20_30'
fromCardId: 20
toCardId: 30
status: valid
---

Le manque de pluie et l’évaporation sont les causes de sécheresses. Dans les deux cas, c’est la perturbation du cycle de l’eau qui est en cause.
