---
linkId: '21_32'
fromCardId: 21
toCardId: 32
status: valid
---

Dans les pays du Nord, une hausse de la température peut améliorer les rendements. Dans les pays du Sud, c'est le contraire : la moindre augmentation de température se traduit par une baisse de rendement.
