---
linkId: '30_12'
fromCardId: 30
toCardId: 12
status: optional
---

Si la pénurie de précipitations se produit au cours de la période de croissance de la végétation, il y a alors une diminution de la photosynthèse et donc une diminution de la capacité du puits de carbone. En Europe, en 2018, les puits de carbone ont baissé de 18 %.
