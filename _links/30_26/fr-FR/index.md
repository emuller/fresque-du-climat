---
linkId: '30_26'
fromCardId: 30
toCardId: 26
status: valid
---

Voir la carte 26 : Si le sol a été durci par une sécheresse, c'est pire car l'eau ruisselle.
