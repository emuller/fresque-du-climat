---
linkId: '33_31'
fromCardId: 33
toCardId: 31
status: optional
---

Si l'eau de mer monte, elle peut pénétrer les nappes phréatiques qui sont des réserves d'eau douce.
