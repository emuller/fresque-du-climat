---
linkId: '35_32'
fromCardId: 35
toCardId: 32
status: optional
---

En général, ce qui brûle bien, c'est la forêt, pas les champs de blé.
