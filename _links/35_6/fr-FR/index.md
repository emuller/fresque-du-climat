---
linkId: '35_6'
fromCardId: 35
toCardId: 6
status: optional
---

Une partie de la déforestation est faite en brûlant la forêt qui peut ensuite dégénérer en feu non-maîtrisé. C'est ce qu'il s'est passé pendant l'été 2019 en Amazonie et en Australie.
