---
linkId: '3_26'
fromCardId: 3
toCardId: 26
status: optional
---

L'artificialisation des sols est également responsable d'inondations car le sol n'est plus drainant.
