---
linkId: '42_12'
fromCardId: 42
toCardId: 12
status: valid
---

Le CO2 est absorbé à la surface de l'océan. La circulation thermohaline a pour effet de mélanger l'eau de surface et l'océan profond, ce qui est indispensable à l'absorption du CO2 par l'océan.
