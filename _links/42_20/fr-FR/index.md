---
linkId: '42_20'
fromCardId: 42
toCardId: 20
status: valid
---

Le ralentissement de la circulation thermohaline va avoir pour effet un dérèglement du cycle de l'eau dans un certain nombre de région du monde.
