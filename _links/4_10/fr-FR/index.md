---
linkId: '4_10'
fromCardId: 4
toCardId: 10
status: optional
---

La pollution locale que représentent les échappements des voitures est une motivation forte pour pousser la voiture électrique. Or cette dernière est responsable d'émissions dues à la génération de l'électricité elle-même (selon le mix électrique du pays considéré).
