---
linkId: '5_40'
fromCardId: 5
toCardId: 40
status: optional
---

Bien que des conflits peuvent avoir lieu à cause des ressources, ce n'est pas directement causé par une perturbation du climat, mais bien par un appauvrissement des ressources. La relation peut-être faite, mais elle est plus politique que climatique.
