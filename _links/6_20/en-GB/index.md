---
linkId: '6_20'
fromCardId: 6
toCardId: 20
status: optional
---

Deforestation can perturb local precipitation.
