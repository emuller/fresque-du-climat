---
linkId: '6_30'
fromCardId: 6
toCardId: 30
status: optional
---

La déforestation peut être la cause directe de sécheresses car les arbres stockent beaucoup d'eau. Si on les coupe, ils ne jouent plus ce rôle de régulateurs de l'humidité.
