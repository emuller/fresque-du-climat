---
linkId: '8_12'
fromCardId: 8
toCardId: 12
status: optional
---

Ce n'est pas grave si ce lien n'est pas fait, mais il est vrai que l'agriculture peut améliorer la capacité de stockage via la photosynthèse. C'est le principe des 4 pour 1000 (si on augmentait ne serait-ce que de 4/1000 la capacité du sol à séquestrer du carbone, on aurait un impact important sur le CO2).
