---
linkId: '8_6'
fromCardId: 8
toCardId: 6
status: valid
---

La déforestation est liée à 80% à l'agriculture, donc oui. Elle peut être considérée comme une activité humaine ou comme une conséquence de l'agriculture ou les deux.
