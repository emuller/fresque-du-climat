---
title: 'À Propos de la Fresque du Climat'
---

[La Fresque du Climat](https://fresqueduclimat.org) est un atelier de sensibilisation où les participants co-construisent une fresque résumant les mécanismes du changement climatique tels qu'expliqués dans les rapports du GIEC.

Ce mémo de la Fresque du Climat a pour but de proposer aux animateurs un accès rapide aux cartes particulièrement sur mobile afin de réviser les liens ou approfondir leurs connaissances.

Vous avez remarqué un bug ou vous avez une amélioration à proposer, contactez-nous sur le canal dédié sur [Telegram](https://t.me/memofdc).
