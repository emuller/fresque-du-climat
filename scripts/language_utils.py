
# https://lingohub.com/developers/supported-locales/language-designators-with-regions

LANGUAGES_DICT = {
    "Arabic (Morocco)": "ar-MA", # Arabic (Maroc)
    "Brazilian Portuguese": "pt-BR", # Portuguese (Brazil)
    "Swedish": "sv-SE", # Swedish (Sweden)
    "Turkish": "tr-TR", # Turkish (Turkey)
    "Ukrainian": "uk-UA",  # Ukrainian (Ukraine)
    "Traditional Arabic": "ar-AR", # Arabic

    "Lithuanian": "lt-LT", # Lithuanian
    "AR-AR": "ar-AR", # Arabic
    "FR-FR": "fr-FR", # France
    "French": "fr-FR", # France
    "EN-GB": "en-GB", # British English
    "British English": "en-GB", # British English
    "EN-EN": "en-GB", # British English
    "DE-DE": "de-DE", # Germany
    "German": "de-DE", # Germany
    "AR-MR": "ar-MR", # Arabic (Mauritania)
    "AR-MA": "ar-MA", # Arabic (Maroc)
    "AR-MC": "ar-MA", # Arabic (Maroc)
    "BG-BG": "bg-BG", # Bulgarian
    "Bulgarian": "bg-BG", # Bulgarian
    "BR-BR": "br-FR", # Breton
    "Breton": "br-FR", # Breton
    "CS-CZ": "cs-CZ", # Czech
    "Czech": "cs-CZ", # Czech
    "DA-DK": "da-DK", # Danish
    "Danish": "da-DK", # Danish
    "EN-US": "en-US", # English (United States)
    "American English": "en-US", # English (United States)
    "ES-ES": "es-ES", # Spanish (Spain)
    "Spanish": "es-ES", # Spanish (Spain)
    "ES-MX": "es-MX", # Spanish (Mexico)
    "Mexican Spanish": "es-MX",  # Spanish (Mexico)
    "EE-ET": "et-EE", # Estonian (Estonia)
    "ET-EE": "et-EE", # Estonian (Estonia)
    "Estonian": "et-EE", # Estonian (Estonia)
    "Basque": "eu-ES", # Basque (Spain)
    "EU-ES": "eu-ES", # Basque (Spain)
    "GR-GR": "el-GR", # Greece
    "EL-GR": "el-GR", #Greece
    "Greek": "el-GR", #Greece
    "HE-IL": "he-IL", # Hebrew (Israel)
    "Hebrew": "he-IL", # Hebrew (Israel)
    "HR-HR": "hr-HR", # Croatian (Croatia)
    "Croatian": "hr-HR", # Croatian (Croatia)
    "HU-HU": "hu-HU", # Hungarian (Hungary)
    "Hungarian": "hu-HU", # Hungarian (Hungary)
    "IT-IT": "it-IT", # Italian (Italy)
    "Italian": "it-IT", # Italian (Italy)
    "JA-JP": "ja-JP", # Japanese (Japan)
    "Japanese": "ja-JP", # Japanese (Japan)
    "Korean": "ko-KR", # Coréen de Corée
    "JP-JP": "ja-JP", # Japanese (Japan)
    "LU-LU": "lb-LU", # Luxembourgeois
    "Luxembourgish": "lb-LU", # Luxembourgeois
    "Latvian": "lv-LV",
    "Malagasy": "mg-MG", # Malagasy
    "NL-BE": "nl-BE", # Dutch (Belgium)
    "Dutch": "nl-BE", # Dutch (Belgium)
    "NL-NLBE": "nl-NL", # Dutch (Belgium)
    "NO-NOR": "no-NO", # Norwegian (Norway)
    "PL-PL": "pl-PL", # Polish (Poland)
    "Polish": "pl-PL", # Polish (Poland)
    "PT-BR": "pt-BR", # Portuguese (Brazil)
    "PT-PT": "pT-PT", # Portuguese (Portugal)
    "Portuguese": "pT-PT", # Portuguese (Portugal)
    "RO-RO": "ro-RO", # Romanian (Romania)
    "Romanian": "ro-RO", # Romanian (Romania)
    "RU-RU": "ru-RU", # Russian (Russia)
    "Russian": "ru-RU", # Russian (Russia)
    "SK-SK": "sk-SK", # Slovak (Slovakia)
    "Slovak": "sk-SK", # Slovak (Slovakia)
    "SR-SP": "sr-SP", # Serbian
    "Serbian": "sr-SP", # Serbian
    "SV-SE": "sv-SE", # Swedish (Sweden)
    "Swedish": "sv-SE", # Swedish (Sweden)
    "TR-TR": "tr-TR", # Turkish (Turkey)
    "UK-UA": "uk-UA", # Ukrainian (Ukraine)
    "Traditional Mandarin (Chinese)": "zh-CN", # Chinese China
    "Chinese Mandarin (simplified)": "zh-CHT",  # simplified
    "ZH-CHT": "zh-CHT", # Chinese simplified
    "ZH-CN": "zh-CN", # Chinese (China),
    "SQ-AL": "sq-AL", #Albanian
    "Albanian": "sq-AL",  # Albanian
    "NB-NO": "nb-NO", #Norwegian Bokmål
    "Norwegian": "nb-NO", #Norwegian Bokmål
    "SL-SL": "sl-SL", # Slovenian,
    "SI-SI": "si-SL", # Singhalais (SriLank)
    "Sinhala": "si-SL",  # Singhalais (SriLank)
    "Slovenian": "sl-SL", # Slovenian,
    "TH-TH": "th-TH", # Thailand, 
    "Thai": "th-TH", # Thailand,
    "NL-NL": "nl-NL", # Dutch (Netherlands), 
    "MK-MK": "mk-MK", # Macedonian (Macedonia)
    "Macedonian": "mk-MK", # Macedonian (Macedonia)
    "SIN-SL": "si-LK", # Sri lanka,
    "CA-ES": "ca-ES", # Catalan (Spain),
    "Catalan": "ca-ES", # Catalan (Spain),
    "OC-OC": "oc-ES", # Occitan (Spain)
    "Occitan": "oc-ES", # Occitan (Spain)
    "UE-ES": "eu-ES", # Basque (Euskara) (Spain),
    "Finnish": "fi-FI", # Finnois (Finlande)
}